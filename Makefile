HTTP_PORT = 8000
HTTP_IF = -b ::

PYTHON3 = python3
ORGC = emacs -Q --batch --eval "(progn (require 'ob-tangle) (dolist (file command-line-args-left) (with-current-buffer (find-file-noselect file) (let ((default-directory (expand-file-name \"site\" default-directory))) (org-babel-tangle)))))"

run: site/index.html
	cd site && $(PYTHON3) -m http.server $(HTTP_IF) $(HTTP_PORT)

site/index.html: js-ver.org
	$(ORGC) $<
